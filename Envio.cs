﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Libre_Julio
{
    public class Envio
    {
        public int NroEnvio { get; set; }
        public int DNIDestinatario { get; set; }
        public int DNIRepartidor { get; set; }
        public enum Estado
        {
            PENDIENTE_ENVIO,
            ENVIADO,
            ULTIMO_TRAMO_DEL_RECORRIDO,
            ENTREGADO
        }
        public Estado EstadoEnvio { get; set; }
        public DateTime EntregaEstimada { get; set; }
        public DateTime FechaEntrega { get; set; }
        public double PesoEnKG { get; set; }
        public bool Programado { get; set; }
        public List<EventoAsociado> EventosAsociados { get; set; }
        public string DescripcionPaquete { get; set; }
        public int GenerarCodigoAutoincremental()
        {
            return NroEnvio += 1;
        }

        public Envio()
        {
            EstadoEnvio = Estado.PENDIENTE_ENVIO;
            NroEnvio = GenerarCodigoAutoincremental();
        }

        public Mensaje ActualizarEnvio(int NroEnvio, EventoAsociado nuevoEvento) {

            EventoAsociado evento = new EventoAsociado();
            EventoAsociado ultimo = EventosAsociados.Last();
            Mensaje msj = new Mensaje();

            if ((nuevoEvento.Envento == EventoAsociado.TipoEnvento.EN_VIAJE) && (ultimo.Envento == EventoAsociado.TipoEnvento.LLEGADA_AL_CENTRO_DE_DISTRIBUCION))
            {
                evento.Envento = EventoAsociado.TipoEnvento.EN_VIAJE;
                evento.FechaEvento = DateTime.Now;
                EstadoEnvio = Estado.ENVIADO;
                EventosAsociados.Add(evento);
                msj.MensajeError = "";
                msj.Error = false;
                return msj;
            }
            if ((nuevoEvento.Envento == EventoAsociado.TipoEnvento.EN_MANOS_DEL_REPARTIDOR) && (ultimo.Envento == EventoAsociado.TipoEnvento.EN_VIAJE))
            {
                evento.Envento = EventoAsociado.TipoEnvento.EN_MANOS_DEL_REPARTIDOR;
                evento.FechaEvento = DateTime.Now;
                EstadoEnvio = Estado.ULTIMO_TRAMO_DEL_RECORRIDO;
                EventosAsociados.Add(evento);
                msj.MensajeError = "";
                msj.Error = false;
                return msj;
            }

            if ((nuevoEvento.Envento == EventoAsociado.TipoEnvento.ENTREGADO)&& (ultimo.Envento == EventoAsociado.TipoEnvento.EN_MANOS_DEL_REPARTIDOR))
            {
                evento.Envento = EventoAsociado.TipoEnvento.ENTREGADO;
                evento.FechaEvento = DateTime.Now;
                EstadoEnvio = Estado.ENTREGADO;
                EventosAsociados.Add(evento);
                msj.MensajeError = "";
                msj.Error = false;
                return msj;
            }
            msj.MensajeError = "Se ha producido un error";
            msj.Error = true;
            return msj;
        }
    }
}
