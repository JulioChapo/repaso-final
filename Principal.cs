﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Libre_Julio
{
    public class Principal
    {
        public List<Envio> Envios = new List<Envio>();
        public List<EventoAsociado> EventosAsociados = new List<EventoAsociado>();
        public List<Repartidor> Repartidores = new List<Repartidor>();
        public List<Destinatario> Destinatarios = new List<Destinatario>();

        public int CargarNuevoEnvio(int dniDestinatario, DateTime fechaEstimada, string descripcionPaquete, double pesoPaquete)
        {
            Envio nuevoEnvio = new Envio();
            EventoAsociado nuevoEvento = new EventoAsociado();
            nuevoEnvio.DNIDestinatario = dniDestinatario;
            nuevoEnvio.DNIRepartidor = 0;
            nuevoEnvio.DescripcionPaquete = descripcionPaquete;
            nuevoEnvio.EntregaEstimada = fechaEstimada;
            nuevoEvento.Envento = EventoAsociado.TipoEnvento.LLEGADA_AL_CENTRO_DE_DISTRIBUCION;
            nuevoEvento.FechaEvento = DateTime.Now;
            nuevoEnvio.Programado = false;
            nuevoEnvio.PesoEnKG = pesoPaquete;
            nuevoEnvio.EventosAsociados.Add(nuevoEvento);
            Envios.Add(nuevoEnvio);
            return nuevoEnvio.NroEnvio;
        }

        public Mensaje ObtenerEnvio(int nroenvio, EventoAsociado nuevoEvento)
        {
            Envio envioencontrado = Envios.Find(x => x.NroEnvio == nroenvio);
            return envioencontrado.ActualizarEnvio(nroenvio, nuevoEvento);  
        }

        public void AsignarPedido()
        {
            List<Envio> noAsignados = Envios.FindAll(x => x.DNIRepartidor == 0);
            noAsignados.OrderByDescending(x => x.PesoEnKG);   

            foreach (var item in noAsignados)
            {
                Destinatario destinatario = Destinatarios.Find(x => x.DNI == item.DNIDestinatario);
                foreach (var repartidor in Repartidores)
                {
                    foreach (var cp in repartidor.CodigosPostalesHabilitados)
                    {
                        if (destinatario.CodigoPostal == repartidor.CodigosPostalesHabilitados[cp])
                        {
                            if (repartidor.CapacidadActualKG + item.PesoEnKG <= repartidor.CapacidadMaximaARepartir)
                            {
                                item.Programado = true;
                                item.DNIRepartidor = repartidor.DNI;
                                repartidor.CapacidadActualKG += item.PesoEnKG;
                                Envios.Add(item);
                            }
                        }
                    }
                }
            }
        }

    }
}
