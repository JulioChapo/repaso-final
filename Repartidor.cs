﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Libre_Julio
{
    public class Repartidor: Persona
    {
        public List<int> CodigosPostalesHabilitados { get; set; }

        public double CapacidadMaximaARepartir { get; set; }
        public double CapacidadActualKG { get; set; }
    }
}
