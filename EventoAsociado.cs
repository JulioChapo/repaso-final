﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Libre_Julio
{
    public class EventoAsociado
    {
        public enum TipoEnvento
        {
            LLEGADA_AL_CENTRO_DE_DISTRIBUCION,
            EN_VIAJE,
            EN_MANOS_DEL_REPARTIDOR,
            ENTREGADO

        }

        public TipoEnvento Envento { get; set; }
        public DateTime FechaEvento { get; set; }
    }
}
