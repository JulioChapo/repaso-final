﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Libre_Julio
{
    public class Destinatario : Persona
    {
        public string Localidad { get; set; }
        public int CodigoPostal { get; set; }
        public string Direccion { get; set; }
    }
}
